from composing import Song, Percussion, Instrument, Scale, Chord, note2number, number2note
import numpy as np
import os
from play_midi import play_midi

percussionsList = [
        Percussion.MARACAS,
        Percussion.HAND_CLAP,
        Percussion.SNARE_ELECTRIC,
        Percussion.LOW_FLOOR_TOM,
        Percussion.HIGH_TOM,
        Percussion.SNARE_ACOUSTIC,
        Percussion.BASS_DRUM_1,
        Percussion.MUTE_HIGH_CONGA,
        Percussion.COWBELL,
        Percussion.HIGH_BONGO,
        Percussion.LOW_BONGO,
        Percussion.MUTE_HIGH_CONGA,
        Percussion.OPEN_HIGH_CONGA,
        Percussion.LOW_CONGA,
        Percussion.HIGH_TIMBALE,
        Percussion.LOW_TIMBALE,
        Percussion.HIGH_AGOGO,
        Percussion.LOW_AGOGO,
        Percussion.CABASA,
        Percussion.CLAVES,
        Percussion.HIGH_WOOD_BLOCK,
        Percussion.LOW_WOOD_BLOCK,
        Percussion.MUTE_CUICA,
        Percussion.OPEN_CUICA,
        Percussion.MUTE_TRIANGLE,
        Percussion.OPEN_TRIANGLE,
        ]

def run():
    song_path = generate_song()
    # Uncomment to play the song in a blocking fashion
    play_midi(song_path)

def generate_song():
    seed = np.random.randint(1000000)
    song_name = name_from_seed(seed)
    bpm = 200
    song = Song(bpm)
    
    beat1 = percussionsList.pop(np.random.randint(len(percussionsList)))
    beat2 = percussionsList.pop(np.random.randint(len(percussionsList)))
    beat3 = Percussion.HAND_CLAP

    drum_track = song.drum_track()
    guitar_track = song.new_track(Instrument.GUITAR_ELECTRIC_CLEAN)

    melody_range = Scale.DIATONIC.start_from("C3")
    current_note = len(melody_range) // 2

    #SLOW DRUM START INTO FASTER RHYTM
    for i in range(0, 16, 8):
        drum_track.add_beat(beat1, i, 1)
        drum_track.add_beat(beat2, i + 4, 1)
    for i in range(16, 32, 4):
        drum_track.add_beat(beat1, i, 1)
        drum_track.add_beat(beat2, i + 2, 1)
    for i in range(32, 80, 2):
        drum_track.add_beat(beat1, i, 1)
        drum_track.add_beat(beat2, i + 1, 1)
    for i in range(48, 64, 4):
        drum_track.add_beat(beat3, i, 1)
    for i in range(64, 80, 2):
        drum_track.add_beat(beat3, i, 1)
    for i in range(80, 112, 1):
        drum_track.add_beat(beat3, i, 1)  
    for i in range(80, 128, 1):
        guitar_track.add_note(melody_range[current_note], i, 1)
        if np.random.uniform() < 0.33:
            guitar_track.add_note(melody_range[current_note], i + 0.5, 1)

        current_note = current_note + np.random.randint(-2, 3)
        current_note = np.clip(current_note, 0, len(melody_range) - 1)
    
    #SLOW DOWN A BIT
    for i in range(128, 176, 2):
        guitar_track.add_note(melody_range[current_note], i, 1)
        if np.random.uniform() < 0.33:
            guitar_track.add_note(melody_range[current_note], i + 0.5, 1)

        current_note = current_note + np.random.randint(-2, 3)
        current_note = np.clip(current_note, 0, len(melody_range) - 1)

    #ADD ANOTHER SMALL FASTER PART
    for i in range(144, 160, 1):
        guitar_track.add_arpeggio(
        Chord.AUGMENTED.start_from(np.random.choice(melody_range)),
        i, 1, 0.3
        )

    ### SAVE SONG AND PRINT NAME ###
    if not os.path.exists('out'):
        os.mkdir('out')
    song_path = 'out/{}.mid'.format(song_name)
    song.save(song_path)
    print('Generated song', song_name)
    # Uncomment to play the song in a blocking fashion
    # song.play()
    return song_path


def name_from_seed(seed):
    with open('assets/nouns.txt') as f:
        nouns = [s.strip() for s in f.readlines()]
    with open('assets/adjectives.txt') as f:
        adjectives = [s.strip() for s in f.readlines()]
    noun = nouns[seed % 1000]
    adjective = adjectives[seed // 1000]
    return '{}_{}'.format(adjective, noun)


if __name__ == '__main__':
    run()
